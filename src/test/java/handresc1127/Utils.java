package handresc1127;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	private static Session session = null;
	private static String emailID = null;

	public Utils() {
		emailSetup();
		sendEmail(session, emailID, "SimpleEmail Testing Subject", "SimpleEmail Testing Body");
	}

	public static void sendImage(String pathImagen) {
		emailSetup();
		sendImageEmail(session, "h.andresc1127@gmail.com", "Sujeto pruebas", "Hola pruebas", pathImagen);
	}

	private static void emailSetup() {
		emailID = "automatizacionmicuenta@gmail.com";
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.auth", "true");
		session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("automatizacionmicuenta@gmail.com", "*****");
			}
		});
	}

	private static void sendEmail(Session session, String toEmail, String subject, String body) {
		try {
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress("automatizacionmicuenta@gmail.com", "Test Automation (NoReply) - By HACC"));
			msg.setReplyTo(InternetAddress.parse("automatizacionmicuenta@gmail.com", false));
			msg.setSubject(subject, "UTF-8");
			msg.setText(body, "UTF-8");
			msg.setSentDate(new Date());

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			LOGGER.info("Message is ready");
			Transport.send(msg);

			LOGGER.info("EMail Sent Successfully!!");
		} catch (Exception e) {
			LOGGER.error("Error sendEmail: ", e);
		}
	}

	private static void sendImageEmail(Session session, String toEmail, String subject, String body, String filename) {
		try {
			MimeMessage msg = new MimeMessage(session);
			DataSource srcImage = new FileDataSource(filename);

			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress("automatizacionmicuenta@gmail.com", "Test Automation (NoReply) - By HACC"));
			msg.setReplyTo(InternetAddress.parse("automatizacionmicuenta@gmail.com", false));
			msg.setSubject(subject, "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(body);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Second part is image attachment
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(srcImage));
			messageBodyPart.setFileName("evidence.png");
			// Trick is to add the content-id header here
			// messageBodyPart.setHeader("Content-ID", "image_id");
			messageBodyPart.setHeader("Content-ID", "<image>");
			multipart.addBodyPart(messageBodyPart);

			// third part for displaying image in the email body
			messageBodyPart = new MimeBodyPart();
			String htmlText = "<H1>Evidencia</H1><img src='cid:image'>";
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
			// Set the multipart message to the email message
			msg.setContent(multipart);

			// Send message
			Transport.send(msg);

			LOGGER.info("EMail Sent Successfully with image!!");
		} catch (Exception e) {
			LOGGER.error("Error sendEmailImage: ", e);
		}

	}

}
