package handresc1127.definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import handresc1127.pages.ebayHomePage;
import handresc1127.pages.ebaySharePage;

public class ebayDefinitions {
	ebayHomePage homePage;
	ebaySharePage sharePage;
	
	@Given("Enter to Ebay")
	public void navegate_to_url() {
		homePage=new ebayHomePage();
	}
	
	@When("Search {string}")
	public void search(String keysToSend) {
		sharePage= new ebaySharePage(homePage.findItem(keysToSend));
	}
	
	@When("Select brand {string}")
	public void select_brand(String brach) {
	    sharePage.chooseBranch(brach);
	}

	@When("Select size {string}")
	public void select_size(String size) {
		sharePage.chooseSize(size);
	}

	@When("Print the number of results")
	public void print_the_number_of_results() {
	    sharePage.printResultsNumber();
	}
	
	@When("Order by {string}")
	public void order_by_price_ascendant(String orderType) {
	    sharePage.orderResultsBy(orderType);
	}
	
	@Then("Assert the order taking the first {int} results")
	public void assert_the_order_taking_the_first_results(int int1) {
	    sharePage.assertFirsts(int1);
	}
	
	@Then("Order and print the products by name")
	public void order_and_print_the_products_by_name() {
		sharePage.orderByColumn(1);
	}

}
