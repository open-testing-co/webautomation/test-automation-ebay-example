package handresc1127.pages;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import handresc1127.Utils;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ebayHomePage {
	
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	 By sharedItems = By.id("gh-ac");
	 By sharedSubmit = By.id("gh-btn");
	 
	@BeforeClass
	    public static void setupClass() {
	        WebDriverManager.chromedriver().setup();
	}
	
	@Before
	public void beforeEach() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		driver = new ChromeDriver(options); 
		driver.get("https://www.ebay.com/");
		
		wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(sharedItems));
	}
	
	public WebDriver findItem(String keysToSend) {
		driver.findElement(sharedItems).sendKeys(keysToSend);
		driver.findElement(sharedSubmit).click();
		return driver;
	}
	
	@After
	public void afterEach() {
		driver.quit();
	}
	@AfterClass
    public static void after(){
		Utils.sendImage(System.getProperty("user.dir") + "\\target\\evidences\\" + "evidence.png");
    }
}
