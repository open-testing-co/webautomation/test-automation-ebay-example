package handresc1127.pages;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.TreeMap;

import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ebaySharePage {
	
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	 By choiceBrach(String brach) {return By.xpath("//*[@id='x-refine__group_1__1']//a[contains(.,'"+brach+"')]//input");}
	 By filters(String filter) {return By.xpath("//*[contains(@class,'srp-carousel-list__item--large-items')][contains(.,'"+filter+"') or contains(.,'filtros aplicados')]");}
	 By choiceSize(String brach) {return By.xpath("//*[@id='x-refine__group_1__0']//a[contains(.,'"+brach+"')]//input");}
	 By results = By.xpath("//li[contains(@id,'srp-river-results-listing')]");
	 By resultPrice = By.cssSelector("*.s-item__price");
	 By resultName = By.cssSelector("*.s-item__title");
	 By resultNumber = By.cssSelector(".srp-controls__count-heading > span:nth-child(1)");
	 By orderSelect = By.id("w9");
	 By orderSelectType(String type) {return By.xpath("//*[@id='w9']//li[contains(.,'"+type+"')]");}
	 
	 public ebaySharePage(WebDriver driver_) {
		driver=driver_;
		wait = new WebDriverWait(driver,30);
	}
	 
	public void chooseBranch(String brach) {
		driver.findElement(choiceBrach(brach)).click();
		wait.until(ExpectedConditions.elementToBeClickable(filters(brach)));
	}

	public void chooseSize(String size) {
		driver.findElement(choiceSize(size)).click();
		wait.until(ExpectedConditions.elementToBeClickable(filters(size)));
	}
	
	public void printResultsNumber() {
		String number=driver.findElement(resultNumber).getText();
		System.out.println("Number of results: "+number);
	}
	
	public void orderResultsBy(String orderType) {
		wait.until(ExpectedConditions.elementToBeClickable(orderSelect));
		WebElement element=driver.findElement(orderSelect);
		String classValue=element.getAttribute("class").trim();
		classValue=classValue+" "+classValue+"--open";
		String script = "arguments[0].setAttribute('class', arguments[1]);";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(script, element, classValue);
		
		wait.until(ExpectedConditions.elementToBeClickable(orderSelectType(orderType)));
		driver.findElement(orderSelectType(orderType)).click();
		printResults();
	}
	
	public void assertFirsts(int limit) {
		List<WebElement> allResults=driver.findElements(results);
		int i=0;
		double lastValue=-1;
		for (WebElement oneResult : allResults) {
			String priceText=oneResult.findElement(resultPrice).getText();
			String priceExtract="";
		    priceExtract=priceText.split(" a ")[0].replace("COP $", "").replace(" ", "");
		    double price;
		    try {
		    	price = Double.parseDouble(priceExtract); }
		    catch(Exception e) {
		    	price = Double.MAX_VALUE;
		    }
		    assertTrue(price > lastValue);
			System.out.println(i+"  "+price);
			i++;
			if(i>=limit) break;
		}
		System.out.println();
	}
	
	public void orderByColumn(int i) {
		List<WebElement> allResults=driver.findElements(results);
		TreeMap<String,WebElement> results = new TreeMap<String,WebElement>();
		for (WebElement oneResult : allResults) {
			String nameText=oneResult.findElement(resultName).getText();
			results.put(nameText, oneResult);
		}
		for (String name: results.keySet()) 
        { 
            System.out.println(name); 
        }
		System.out.println();
	}
	
	
	public void printResults() {
		List<WebElement> allResults=driver.findElements(results);
		int i=0;
		for (WebElement oneResult : allResults) {
			System.out.println(i+"  "+oneResult.getText());
			i++;
		}
		System.out.println();
	}
	
	
	public static void selectContains(WebDriver driver, By by, String valueContains) {
		WebElement element = driver.findElement(by);
		String valueComboBox = element.getText();
		assertThat(valueComboBox, CoreMatchers.containsString(valueContains));
		String[] values = valueComboBox.split("\n");
		int index = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i].contains(valueContains)) {
				index = i;
				break;
			}
		}

		Select select = new Select(element);
		select.selectByIndex(index);
		WebElement option = select.getFirstSelectedOption();
		String valorActual = option.getText();
		if (!valorActual.contains(valueContains)) {
			select = new Select(element);
			select.selectByIndex(index + 1);
			option = select.getFirstSelectedOption();
			valorActual = option.getText();
		}
		assertThat(valorActual, CoreMatchers.containsString(valueContains));
	}
	
	
	
	

}
