#Author: h.andresc1127@gmail.com

Feature: Test Automation Ebay
  As a Senior Test Automation
  I want make a demo of test automation
  So that show the test automation power

  Scenario: Title of your scenario
    Given Enter to Ebay
    When Search "shoes"
    And Select brand "PUMA"
    #there are different between "10 " and "10.5"
    And Select size "10 "
    And Print the number of results
    And Order by "bajo primero"
    Then Assert the order taking the first 5 results
    #This is a manual order, page has not this option
    And Order and print the products by name
    And Order by "alto primero"
    #And Report should be sent by mail.
